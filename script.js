var crsr = document.querySelector("#cursor");
var blur = document.querySelector("#cursor-blur");

document.addEventListener("mousemove", function (dets) {
  crsr.style.left = dets.x + "px";
  crsr.style.top = dets.y + "px";
  blur.style.left = dets.x - 150 + "px";
  blur.style.top = dets.y - 150 + "px";
}),
  gsap.to("#nav", {
    backgroundColor: "#000",
    height: "95px",
    duration: 0.1,
    scrollTrigger: {
      trigger: "#nav",
      scroller: "body",
      // markers: true,
      start: "top 1%",
      end: "top -30%",
      scrub: 1,
    },
  });

gsap.to("#main", {
  backgroundColor: "#000",
  scrollTrigger: {
    trigger: "#main",
    //scroller: "body",
    markers: true,
    start: "top -35vh",
    end: "top -70%",
    scrub: 1,
  },
});

gsap.from("#colon1", {
  y: -100,
  x: -100,
  scrollTrigger: {
    trigger: "#colon1",
    scroller: "body",

    start: "top 55%",
    end: "top 45%",
    scrub: 3,
  },
});
gsap.from("#colon2", {
  // y: 70,
  // x: 70,
  // scrollTrigger: {
  //   trigger: "#colon1",
  //   scroller: "body",
  //   // markers:true,
  //   start: "top 55%",
  //   end: "top 45%",
  //   scrub: 4,
  // },
});

gsap.from("#page4 h1", {
  y: 50,
  scrollTrigger: {
    trigger: "#page4 h1",
    scroller: "body",
    markers: true,
    start: "top 100%",
    end: "top 70%",
    scrub: 3,
  },
});
